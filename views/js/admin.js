/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *  @author    Veebipoed.ee, Holmbank
 *  @copyright 2023 Veebipoed.ee, Holmbank
 *  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

// Insert loaded values from API.
$(document).ready(function() {
    let paymentOption = $('#type').val();

    // Initially fill fields for all languages
    fillFieldsForAllLanguages(paymentOption);

    // Update fields on type change
    $('#type').on('change', function(e) {
        let paymentOption = $('#type').val();
        fillFieldsForAllLanguages(paymentOption);
    });
});

// Function to fill fields for all languages
function fillFieldsForAllLanguages(paymentOption) {
    for (const [key, value] of Object.entries(loanProductTypes)) {
        if (value.type == paymentOption) {

            $('[id^="name_"], [id^="logo_"]').each(function() {
                const fieldId = $(this).attr('id');
                const fieldName = fieldId.split('_')[0];
                const currentValue = $(this).val();

                if (fieldName === 'name' && ($(this).val() == '' || Object.values(loanProductTypes).some(product => product.name === currentValue))) {
                    $(this).val(value.name);
                }
                if (fieldName === 'logo' && ($(this).val() == '' || Object.values(loanProductTypes).some(product => product.logoUrl === currentValue))) {
                    $(this).val(value.logoUrl);
                }
            });
        }
    }
}

