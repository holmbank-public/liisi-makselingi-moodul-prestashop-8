<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *  @author    Veebipoed.ee, Holmbank
 *  @copyright 2023 Veebipoed.ee, Holmbank
 *  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

$sql_queries = array();

$sql_queries[] = 'DROP TABLE IF EXISTS ' . _DB_PREFIX_ . 'holmbank_merchant_product';
$sql_queries[] = 'DROP TABLE IF EXISTS ' . _DB_PREFIX_ . 'holmbank_orders';

foreach ($sql_queries as $query)
{
    if (!Db::getInstance()->execute($query))
    {
        return false;
    }
}

return true;
