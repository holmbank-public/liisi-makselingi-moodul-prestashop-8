<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 *  @author    Veebipoed.ee, Holmbank
 *  @copyright 2023 Veebipoed.ee, Holmbank
 *  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

if (!class_exists('HolmbankApi'))
{
    include(_PS_MODULE_DIR_.'holmbank/src/api/HolmbankApi.php');
}


/**
 * Repository to process Database operations with Holmbank Order Table.
 */
class OrderRepository
{
    /**
     * Method process insertion of holmbank order in Database.
     * @param $orderData array Holmbank order data to be inserted in Database.
     * @return mixed boolean as indicator of successful operation.
     */
    public function addHolmbankOrder($orderData)
    {
        return Db::getInstance()->insert('holmbank_orders', [
            'id_shop' => (int) $orderData['id_shop'],
            'id_merchant_order' => (int) $orderData['id_merchant_order'],
            'id_holmbank_order' => pSQL($orderData['id_holmbank_order']),
            'x_payment_link_req_id' => pSQL($orderData['x_payment_link_req_id']),
        ]);
    }

    /**
     * Method process search among holmbank orders to find needed by holmbank order ID.
     * @param $orderId string Holmbank order ID.
     * @return mixed boolean as indicator of successful operation.
     */
    public function getHolmbankOrderById($orderId)
    {
        $query = 'SELECT * FROM `%1$sholmbank_orders` WHERE `id_holmbank_order`="%2$s"';
        return Db::getInstance()->getRow(sprintf($query, _DB_PREFIX_, pSQL($orderId)));
    }

    /**
     * Method process search among holmbank orders to find needed by holmbank order ID and secret key.
     * @param $orderId string Holmbank order ID.
     * @param $uniqueKey string Transaction related secret key.
     * @return mixed boolean as indicator of successful operation.
     */
    public function getHolmbankOrderByIdAndKey($orderId, $uniqueKey)
    {
        $query = 'SELECT * FROM `%1$sholmbank_orders` WHERE `id_holmbank_order`="%2$s" AND `x_payment_link_req_id`="%3$s"';
        return Db::getInstance()->getRow(sprintf($query, _DB_PREFIX_, pSQL($orderId), pSQL($uniqueKey)));
    }
}